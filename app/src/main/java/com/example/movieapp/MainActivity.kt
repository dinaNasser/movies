package com.example.movieapp

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowInsetsController
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.`interface`.LoadingCallback
import com.example.movieapp.adapter.MovieAdapter
import com.example.movieapp.viewModel.MovieViewModel
import com.github.ybq.android.spinkit.SpinKitView

class MainActivity : AppCompatActivity(), LoadingCallback {

    private lateinit var recyclerAdapter: MovieAdapter
    private lateinit var loading: SpinKitView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
        toolbar()
        initLoading()
        initViewModel()


    }

    /**
     * loading until request well done
     */
    private fun initLoading() {
        loading = findViewById<SpinKitView>(R.id.spin_kit)
    }

    /**
     * change color of status bar
     */
    private fun toolbar() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            window.insetsController?.setSystemBarsAppearance(
                WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS or
                        WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS,
                WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
            )
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                @Suppress("DEPRECATION")
                window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR

            }
        }
    }

    /**
     * handle progress bar of download of file
     * on click on download icon ber item
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun handleLoad(
        loadBar: ProgressBar,
        tvLoad: TextView,
        icon: ImageView,
        url: String?,
        type: String
    ) {
        val secondaryHandler = Handler()
        var primaryProgressStatus = 0

        Thread(Runnable {
            while (primaryProgressStatus < 100) {
                primaryProgressStatus += 1
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                secondaryHandler?.post {

                    loadBar.progress = primaryProgressStatus
                    tvLoad.text = "$primaryProgressStatus% of 100"

                    if (primaryProgressStatus >= 100) {
                        tvLoad.text = "load completed"
                        icon.setImageDrawable(getDrawable(R.drawable.ic_done))
                    }
                }
            }
        }).start()
    }

    /**
     * -show loading before make request
     * -call api
     * -get data in call back , hide loading
     * -update adapter by list change
     */
    private fun initViewModel() {
        val viewModel: MovieViewModel = ViewModelProvider(this).get(MovieViewModel::class.java)
        viewModel.getLiveDataObserver().observe(this, {
            if (it != null) {
                loading.visibility = View.GONE

                recyclerAdapter.setMovieList(it)
                recyclerAdapter.notifyDataSetChanged()
            } else {
                loading.visibility = View.GONE
                Toast.makeText(this, "error in get list", Toast.LENGTH_LONG).show()
            }
        })
        loading.visibility = View.VISIBLE
        viewModel.makeApiCall()
    }

    /**
     * define recycler view
     */
    private fun initRecyclerView() {

        var recyclerView = findViewById<RecyclerView>(R.id.movie_recycler)

        LinearLayoutManager(this).also {
            recyclerView
                .layoutManager = it
        }

        recyclerAdapter = MovieAdapter(this, this)
        recyclerView.adapter = recyclerAdapter
    }
}