package com.example.movieapp.adapter


import android.app.Activity
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.`interface`.LoadingCallback
import com.example.movieapp.data.MovieModel

class MovieAdapter(var activity: Activity, var callback: LoadingCallback) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private var movieList: List<MovieModel>? = null

    fun setMovieList(list: List<MovieModel>?) {
        this.movieList = list;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        // Inflate the custom layout
        val contactView = LayoutInflater.from(context).inflate(R.layout.row_movie, parent, false)
        return ViewHolder(contactView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList?.get(position)!!, activity, callback)
    }

    override fun getItemCount(): Int {
        if (movieList == null) return 0
        else
            return movieList?.size!!
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val icon: ImageView = itemView.findViewById(R.id.img_load)
        private val name: TextView = itemView.findViewById(R.id.tv_name)
        private val type: TextView = itemView.findViewById(R.id.tv_type)
        private val loadBar: ProgressBar = itemView.findViewById(R.id.load_bar)
        private val tvLoad: TextView = itemView.findViewById(R.id.tv_load)
        private val img: ImageView = itemView.findViewById(R.id.img)

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bind(data: MovieModel, activity: Activity, callback: LoadingCallback) {
            name.text = data.name
            type.text = data.type

            if (data.type == "PDF")
                img.setImageDrawable(activity.getDrawable(R.drawable.ic_pdf))

            icon.setOnClickListener(View.OnClickListener {
                loadBar.visibility = View.VISIBLE
                data.type?.let { it1 -> callback.handleLoad(loadBar, tvLoad, icon, data.url, it1) }
                icon.isClickable = false
            })
        }
    }


}