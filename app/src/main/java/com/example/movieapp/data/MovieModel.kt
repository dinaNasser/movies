package com.example.movieapp.data

data class MovieModel(
    val url: String?,
    val name: String?,
    val type: String?
)