package com.example.movieapp.`interface`

import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView

interface LoadingCallback {
    fun handleLoad(
        loadBar: ProgressBar,
        tvLoad: TextView,
        icon: ImageView,
        url: String?,
        type: String
    )
}