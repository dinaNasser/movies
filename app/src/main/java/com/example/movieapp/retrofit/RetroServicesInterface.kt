package com.example.movieapp.retrofit

import com.example.movieapp.data.MovieModel
import io.reactivex.Observable
import retrofit2.http.GET

interface RetroServicesInterface {
    @GET("movies")
    fun getMovieList(): Observable<List<MovieModel>>
}