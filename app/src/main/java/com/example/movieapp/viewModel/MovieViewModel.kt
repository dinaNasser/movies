package com.example.movieapp.viewModel


import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movieapp.data.MovieModel
import com.example.movieapp.retrofit.RetroInstance
import com.example.movieapp.retrofit.RetroServicesInterface
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class MovieViewModel() : ViewModel() {
    val TAG = "MainViewModel"

    var LiveDataList: MutableLiveData<List<MovieModel>>

    init {
        LiveDataList = MutableLiveData()
    }

    fun getLiveDataObserver(): MutableLiveData<List<MovieModel>> {
        return LiveDataList
    }

    fun makeApiCall() {
        val retroInstance = RetroInstance.getRetroInstance()
        val retroServices = retroInstance.create(RetroServicesInterface::class.java)
        retroServices.getMovieList()?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableObserver<List<MovieModel>>() {
                override fun onNext(value: List<MovieModel>?) {
                    LiveDataList.postValue(value)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "error=" + e.message)
                    //if request has error
                    LiveDataList.postValue(null)
                }

                override fun onComplete() {
                }
            })


    }
}
